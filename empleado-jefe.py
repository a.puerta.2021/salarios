
class Empleado:
    """Un ejemplo de clase para empleados"""
    def __init__(self, n, s) :
        self.nombre = n
        self.nomina = s

    def calculoImpuestos(self):
        impuestos = self.nomina*0.3
        return impuestos

    def __str__(self):        
        return ("El empleado {name} debe pagar {tax:.1f}".format(name=self.nombre, tax = self.calculoImpuestos()))
    
class Jefe(Empleado):
    def __init__(self, n, s, extra = 0) :
        super().__init__(n,s)
        self.bonus = extra
    
    def __str__(self):
        return ("El jefe {name} debe pagar {tax:.1f}".format(name=self.nombre, tax = self.calculoImpuestos()))

e1 = Empleado("Pepe", 20000)
j1 = Jefe("ana", 30000, 2000)

print(e1)
print(j1)

