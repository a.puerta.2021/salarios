def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))

def calculo_impuestos(nombre, nom):
    impuestos = nom*0.3
    print("el empleado{name} debe pagar {tax:.2f").format(name=nombre,tax=nom)
    return impuestos

class Empleado:
    """Un ejemplo de clase para empleados"""
    def __init__(self, n, s) :
        self.nombre = n
        self.nomina = s

    

empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana",30000)

displayCost(calculo_impuestos(empleadoPepe.nombre, empleadoPepe.nomina) + calculo_impuestos(empleadaAna.nombre, empleadaAna.nombre))