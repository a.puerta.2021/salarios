import unittest
from empleado import Empleado
class Empleado:
    """Un ejemplo de clase para empleados"""
    def __init__(self, n, s) :
        self.nombre = n
        self.nomina = s

    def calculoImpuestos(self):
        impuestos = self.nomina*0.3
        return impuestos

    def __str__(self):        
        return ("el empleado{name} debe pagar {tax:.2f}".format(name=self.nombre, tax = self.calculoImpuestos()))

class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1 = Empleado("nombre", 5000)
        self.assertEqual(e1.calculoImpuestos(), 5)
    
    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calculoImpuestos(), 1500)

    def test_str(self):
        e1 = Empleado("pepe", 50000)
        self.assertEqual("El empleado pepe debe pagar 15000.0", e1.__str__())

if __name__ == "__main__":
    unittest.main()
