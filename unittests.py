
import unittest
from empleado import Empleado

class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1 = Empleado("nombre", 5000)
        self.assertEqual(e1.calculoImpuestos(), 1500)
    
    def test_str(self):
        e1 = Empleado("pepe", 50000)
        self.assertEqual("El empleado pepe debe pagar 15000.0", e1.__str__())

if __name__ == "__main__":
    unittest.main()
