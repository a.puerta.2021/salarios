
class Empleado:
    """Un ejemplo de clase para empleados"""
    def __init__(self, n, s) :
        self.nombre = n
        self.nomina = s

    def calculoImpuestos(self):
        impuestos = self.nomina*0.3
        return impuestos

    def imprime(self):
        tmp = self.calculoImpuestos()
        print("el empleado{name} debe pagar {tax:.2f}".format(name=self.nombre,tax=tmp))

empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana",30000)

total = empleadoPepe.calculoImpuestos()+empleadaAna.calculoImpuestos()

empleadoPepe.imprime()
empleadaAna.imprime()
print("los impuestos a pagar en total son {:.2f} euros".format(total))

