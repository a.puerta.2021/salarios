def coste_impuestos(nomina_empleado, porcentaje_impuesto):
   
    return  (porcentaje_impuesto * nomina_empleado)

def coste_salarial(nomina, impuestos):

    return(nomina + impuestos) 

nominas = {"ana": [30000, 0.25, 350], "pepe": [20000, 0.30, 368], "lucas": [35000, 0.20, 789], "maría": [25000, 0.30, 150]}   

for empleado in nominas:
    print(empleado, ":")    
    print("Impuestos a pagar: " , coste_impuestos(nominas[empleado][0],nominas[empleado][1]))
    if nominas[empleado][2] >365:
        print("Coste salarial(con más de un año de antigüedad): ", 0.9*(coste_salarial(nominas[empleado][0], coste_impuestos(nominas[empleado][0], nominas[empleado][1]))))

    else:
        print("Coste salarial: ", coste_salarial(nominas[empleado][0], coste_impuestos(nominas[empleado][0], nominas[empleado][1])))
