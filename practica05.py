
class Empleado:
    """Un ejemplo de clase para empleados"""
    def __init__(self, n, s) :
        self.nombre = n
        self.nomina = s

    def calculoImpuestos(self):
        impuestos = self.nomina*0.3
        return impuestos

    def __str__(self):        
        return ("el empleado{name} debe pagar {tax:.2f}".format(name=self.nombre, tax = self.calculoImpuestos()))

empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana",30000)

total = empleadoPepe.calculoImpuestos()+empleadaAna.calculoImpuestos()

print(empleadoPepe)
print(empleadaAna)
print("los impuestos a pagar en total son {:.2f} euros".format(total))

